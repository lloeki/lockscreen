all: lockscreen

lockscreen: main.m
	clang -F /System/Library/PrivateFrameworks -framework login -framework Foundation -framework CoreGraphics main.m -o lockscreen

app: lockscreen lock.icns Info.plist
	mkdir -p dist/"Lock Screen.app"/Contents/{MacOS,Resources}
	cp Info.plist dist/"Lock Screen.app"/Contents
	cp lock.icns dist/"Lock Screen.app"/Contents/Resources
	cp lockscreen dist/"Lock Screen.app"/Contents/MacOS

clean:
	rm -rf lockscreen dist/*

.PHONY: clean app
