#!/usr/bin/env ruby
# frozen_string_literal: true

require 'time'

def data
  @data ||= File
            .read(File.join(ENV['HOME'], '.lockscreen.log'))
            .lines
            .map(&:chomp)
            .reject(&:empty?)
            .map { |l| l.split(',') }
            .map { |t, e| { time: Time.parse(t), event: e.to_sym } }
end

def reload!
  @data = nil
end

def debug?
  ENV['DEBUG'] == '1'
end

def yesterday(rows)
  rows.select { |h| h[:time] < Date.today.to_time && h[:time] >= (Date.today - 1).to_time }
end

def today(rows)
  rows.select { |h| h[:time] > Date.today.to_time }
end

def not_today(rows)
  rows.select { |h| h[:time] < Date.today.to_time }
end

def chunk_per_day(rows)
  rows.chunk { |h| h[:time].to_date }
end

def chunk_per_week(rows)
  rows.chunk { |h| h[:time].to_date.tap { |t| break [t.cwyear, t.cweek] } }
end

def chunk_per_month(rows)
  rows.chunk { |h| h[:time].to_date.tap { |t| break [t.year, t.month] } }
end

HOURS = 3600
MINS = 60
WORK_TIME = 37.5 * HOURS
WORK_DAYS = 5

def offset_week(offset, from = Date.today)
  return [from.cwyear, from.cweek - offset] if from.cweek - offset > 0

  offset_week(offset - from.cweek, from.downto(Date.new(from.cwyear) - 4).find { |d| d.cwyear != from.cwyear })
end

def past_week(rows, offset)
  chunk_per_week(rows).select { |(y, w), _| offset_week(offset) == [y, w] }.last&.last || []
end

def current_week(rows)
  past_week(rows, 0)
end

def last_week(rows)
  past_week(rows, 1)
end

def past_month(rows, offset)
  chunk_per_month(rows).select { |(y, m), _| [y, m] == [Date.today.year - (offset - Date.today.month) / 12 - 1, (Date.today.month - offset - 1) % 12 + 1] }.last&.last || []
end

def current_month(rows)
  past_month(rows, 0)
end

def last_month(rows)
  past_month(rows, 1)
end

def remaining_smooth(rows)
  (WEEK_TIME - (presence(current_week(rows)) - presence(today(rows)))) / remaining_days - presence(today(rows))
end

def humanize(obj)
  case obj
  when Float
    secs = obj.to_i.abs
    format('%s%dh %02dm %02ds', obj.negative? ? '-' : '', secs / 3600, secs % 3600 / 60, secs % 60)
  when Date
    obj.to_s
  end
end

def worked(rows)
  chunk_per_day(rows).map(&:first)
end

def insane(rows)
  states(rows).select { |s| s[:period].last - s[:period].first < 15 || s[:period].last.to_date != s[:period].first.to_date && s[:state] == :unlock }
end

def edges(rows)
  rows.slice_when { |a, b| a[:event] != b[:event] }.map { |l| l.first[:event] == :lock ? l.last : l.first }.flatten
end

def segments(rows)
  edges(rows).each_with_object([]) { |e, s| s << [s.last&.last, e] }
end

def states(rows, current: true)
  segs = segments(rows)
  t = segs.map { |s| { period: [s.first&.fetch(:time), s.last[:time]], state: s.first&.fetch(:event) } }
  t << { period: [segs.last.last[:time], Time.now], state: segs.last.last[:event] } if current && segs.last && segs.last.last[:time].to_date == Date.today
  t.drop(1)
end

def events(states)
  sts = states
  e = sts.each_with_object([]) { |s, o| o << { time: s[:period].first, event: s[:state] } }
  if (s = sts.last)
    e << { time: s[:period].last, event: s[:state] == :lock ? :unlock : :lock }
  end
  e
end

def duration(a, b = nil)
  return b[:time] - a[:time] if b

  a[:period].last - a[:period].first
end

def locked(states)
  states.select { |e| e[:state] == :lock }
end

def unlocked(states)
  states.select { |e| e[:state] == :unlock }
end

def sum(states)
  states.map { |e| e[:period].last - e[:period].first }.reduce(&:+)
end

def noon_pause(rows)
  chunk_per_day(rows).map do |_, e|
    locked(states(e)).select { |s| s[:period].first < s[:period].first.to_date.to_time + (14 * HOURS + 30 * MINS) && s[:period].last > s[:period].last.to_date.to_time + (11 * HOURS + 30 * MINS) }.select { |s| duration(s) > 10 * MINS }.max { |a, b| duration(a) <=> duration(b) }
  end.compact
end

def unlock(rows)
  rows.select { |e| e[:event] == :unlock }
end

def lock(rows)
  rows.select { |e| e[:event] == :lock }
end

def first_unlock(rows)
  unlock(rows).min_by { |a| a[:time] }
end

def last_lock(rows)
  lock(rows).max_by { |a| a[:time] }
end

def bounds(rows)
  chunk_per_day(rows).map { |_, e| [first_unlock(e), *events(noon_pause(e)), last_lock(e)].uniq.compact }
end

def late_in(rows)
  unlock(rows).select { |e| e[:time] > e[:time].to_date.to_time + (9 * HOURS + 0 * MINS) && e[:time] < e[:time].to_date.to_time + (12 * HOURS + 0 * MINS) }
end

def presence_time(rows, current: true)
  sum unlocked states rows, current: current
end

def morning_presence?(rows)
  rows.any? { |e| e[:time] < e[:time].to_date.to_time + 12 * HOURS }
end

def afternoon_presence?(rows)
  rows.any? { |e| e[:time] > e[:time].to_date.to_time + 12 * HOURS }
end

def detected_presence(rows)
  chunk_per_day(rows).map { |d, e| [d, (morning_presence?(e) ? 0.5 : 0) + (afternoon_presence?(e) ? 0.5 : 0)] }
end

def expected_presence(rows)
  detected_presence(rows).map(&:last).reduce(&:+)
end

def expected_time(rows)
  WORK_TIME * expected_presence(rows) / WORK_DAYS
end

def remaining_time(rows)
  expected_time(rows) - presence_time(rows)
end

def leave_after(rows)
  Time.now + remaining_time(current_week(rows))
end

def dates(rows)
  rows.map do |e|
    e[:time].strftime('%Y-%m-%d') if e
  end
end

def hours(rows)
  rows.map do |e|
    e[:time].strftime('%H:%M:%S') if e
  end
end

def wdays(rows)
  rows.map do |e|
    e[:time].strftime('%a') if e
  end
end

def weekday?(f)
  !f.saturday? && !f.sunday?
end

def export(rows)
  prev = nil
  off_days = 0
  chunk_per_day(rows).each do |curr, d|
    break if curr > Date.today

    prev = Date.new(curr.year, curr.month, 1) - 1 if prev.nil? && curr.day != 1
    if prev && curr != prev + 1
      (prev + 1).upto(curr - 1).each do |f|
        puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s', *dates([{ time: f.to_time }]), *wdays([{ time: f.to_time }]), *hours([nil, nil, nil, nil]))

        off_days += 1 if weekday?(f) && f.to_time < Time.now
      end
    end
    prev = curr

    b = bounds(d).first
    if morning_presence?(b) && afternoon_presence?(b)
      ams, ame, pms, pme = b
    elsif morning_presence?(b)
      ams, ame, pms, pme = [b[0], b[1], nil, nil]
    elsif afternoon_presence?(b)
      ams, ame, pms, pme = [nil, nil, b[0], b[1]]
    end

    puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', *dates([d.first]), *wdays([d.first]), *hours([ams, ame, pms, pme]), humanize(presence_time(d)))

    off_days += 1 - expected_presence(b)
  end

  if (prev + 1).day != 1
    curr = Date.new(prev.next_month.year, prev.next_month.month, 1)
    (prev + 1).upto(curr - 1).each do |f|
      break if curr > Date.today

      puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s', *dates([{ time: f.to_time }]), *wdays([{ time: f.to_time }]), *hours([nil, nil, nil, nil]))

      off_days += 1 if weekday?(f) && f.to_time < Time.now
    end
  end

  work_time = presence_time(rows)
  off_time = WORK_TIME * off_days / WORK_DAYS
  total_time = work_time + off_time
  baseline = 151.0 * HOURS
  overtime = total_time - baseline
  puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', 'work time', nil, nil, nil, nil, nil, humanize(work_time))
  puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', 'off time', nil, nil, nil, nil, nil, humanize(off_time))
  puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', 'total time', nil, nil, nil, nil, nil, humanize(total_time))
  puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', 'baseline', nil, nil, nil, nil, nil, humanize(baseline))
  puts format('% 10s  % 3s  % 8s  % 8s  % 8s  % 8s  % 14s', 'overtime', nil, nil, nil, nil, nil, humanize(overtime))
end

if $0 == __FILE__
  case ARGV[0]
  when '-i'
    require 'pry'
    binding.pry # rubocop:disable Lint/Debugger
  when '-m'
    ARGV[1] ? export(past_month(data, Integer(ARGV[1]))) : export(current_month(data))
  else
    puts format('%s => %s', humanize(remaining_time(current_week(data))), leave_after(data))
  end
end
