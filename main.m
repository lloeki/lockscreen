#import <objc/runtime.h>
#import <Foundation/Foundation.h>
#import <ApplicationServices/ApplicationServices.h>

#include <sys/syslimits.h>

extern int SACLockScreenImmediate();

bool isLocked() {
    bool result;
    CFNumberRef locked;
    CFDictionaryRef currentDict = CGSessionCopyCurrentDictionary();

    if (!CFDictionaryGetValueIfPresent(currentDict, CFSTR("CGSSessionScreenIsLocked"), (const void**)&locked)) {
        result = false;
    } else {
        int l = 0;
        CFNumberGetValue(locked, kCFNumberIntType, &l);
        if (l > 0) {
            result = true;
        } else {
            result = false;
        }
    }

    CFRelease(currentDict);
    return result;
}

void lock() {
    SACLockScreenImmediate();
}

void waitForLock() {
    for (;;) {
        if (isLocked()) { break; }
        sleep(1);
    }
}

void waitForUnlock() {
    for (;;) {
        if (!isLocked()) { break; }
        sleep(1);
    }
}

void stamp(int log, const char *tag) {
    if (log == -1) { return; }

    time_t rawtime;
    struct tm *info;
    char buffer[80];

    time(&rawtime);
    info = localtime(&rawtime);
    strftime(buffer, 80, "%Y-%m-%dT%H:%M:%S%z", info);
    dprintf(log, "%s,%s\n", buffer, tag);
}

int main (int argc, char *argv[]) {
    bool wait = false;
    if (argc > 1 && strncmp(argv[1], "-w", 2) == 0) {
        wait = true;
    }

    char *home = getenv("HOME");
    char *logFilename = "";
    if (argc > 2 && strncmp(argv[2], "-l", 2) == 0) {
        if (strlen(home) != 0) {
            asprintf(&logFilename, "%s/.lockscreen.log", home);
        }

        if (argc > 3 && strlen(argv[3]) > 0 && argv[3][0] != '-') {
            logFilename = argv[3];
        }
    }

    int log = -1;
    if (strlen(logFilename) != 0) {
        log = open(logFilename, O_WRONLY | O_APPEND | O_CREAT, 0600);
        if (flock(log, LOCK_EX | LOCK_NB) != 0) {
            dprintf(2, "lock failed: %s\n", logFilename);
            return 1;
        }
    }

    lock();
    waitForLock();
    stamp(log, "lock");
    if (wait) { waitForUnlock(); }
    stamp(log, "unlock");

    if (log) { close(log); }
    return 0;
}
